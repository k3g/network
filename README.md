## HOW TO INSTALL DNSMASQ WITH NIP.IO
This example customize powerdns with nip.io for the domain panda.lan
### INSTALL NIP.IO

- install packages pdns-server pdns-backend-pipe (powerdns server & powerdns backend pipe)
- copy the backend pipe from nip.io/usr/local/bin/ to /usr/local/bin/
- copy the powerdns config from  nip.io/etc/powerdns/ to /etc/{powerdns|pdns}/
- disable the powerdns extra config in /etc/{powerdns|pdns}/pdns.d/
- restart powerdns

test with dig (on the same computer as 127.0.0.1)
```
dig yolo.6.6.6.6.panda.lan @127.0.0.1 -p 5300 +noall +answer
```
reply
```
yolo.6.6.6.6.panda.lan.	432000	IN	A	6.6.6.6
```

### INSTALL DNSMASQ

- install package dnsmasq
- copy the conf from dnsmasq/etc/ to /etc/
- restart dnsmaq

test with dig
```
dig yolo.6.6.6.6.panda.lan @127.0.0.1 +noall +answer
```
reply
```
yolo.6.6.6.6.panda.lan. 432000  IN  A   6.6.6.6
```

and
```
dig yolo.cluster.local @127.0.0.1 +noall +answer
```
reply
```
yolo.cluster.local 0 IN  A   127.0.0.1
```

and
```
dig www.google.com @127.0.0.1 +noall +answer
```
reply something like this (ip may be different)
```
www.google.com.		1429	IN	A	216.58.204.132
```
